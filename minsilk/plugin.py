import os
import pcbnew
import re
import wx
import logging


from .dialog import ConfigDialog, Config


class Plugin(pcbnew.ActionPlugin):
    ORIGIN = pcbnew.VECTOR2I(0, 0)
    logger = logging.getLogger(__name__)
    log_handler = None

    def __init__(self):
        super(Plugin, self).__init__()
        self.name = "MinSilk"
        self.category = "Modify PCB"
        self.show_toolbar_button = True
        icon_dir = os.path.dirname(__file__)
        self.icon_file_name = os.path.join(icon_dir, "icon.png")
        self.description = "Make certain silk lines wider"
        self.layers = 0

        self.kicad_version = None
        try:
            version = re.search("\\d+\\.\\d+\\.\\d+",
                                pcbnew.Version()).group(0)
            self.kicad_version = list(map(int, version.split('.')))
        except Exception:
            pass

        self.config = Config()

    def defaults(self):
        pass

    def _setup_logging(self, board_file: str):
        self.logger.propagate = False

        if self.log_handler is not None:
            self.log_handler.close()
            self.logger.removeHandler(self.log_handler)
            self.log_handler = None
            return

        if self.config.debug_log:
            fn = os.path.join(os.path.dirname(board_file), "minsilk.log")
            self.log_handler = logging.FileHandler(fn, "w")
            self.log_handler.setLevel(logging.DEBUG)
            self.log_handler.setFormatter(
                logging.Formatter('%(asctime)s %(levelname)s: %(message)s'))
            self.logger.addHandler(self.log_handler)
            self.logger.setLevel(logging.DEBUG)
            self.logger.info("Log handlers: %s", self.logger.handlers)

    def _check_layer(self, layer):
        return (self.layers & (1 << layer)) != 0

    def _transform_shape(self, shape: pcbnew.PCB_SHAPE):
        if self._check_layer(shape.GetLayer()):
            self.logger.debug("Transforming shape type %s", shape.ShowShape())

            if not shape.Cast().IsFilled():
                width = shape.GetWidth()

                if width and width < self.config.minsize:
                    self.logger.debug("Setting width %d -> %d",
                                      width, self.config.minsize)
                    shape.SetWidth(self.config.minsize)

    def _transform_fp_shape(self, shape: pcbnew.FP_SHAPE):
        if self._check_layer(shape.GetLayer()):
            shape_type = shape.GetShape()
            self.logger.debug("Transforminig footprint shape type %d",
                              shape_type)

            if not shape.IsFilled():
                width = shape.GetWidth()

                if width and width < self.config.minsize:
                    self.logger.debug("Setting width %d -> %d",
                                      width, self.config.minsize)
                    shape.SetWidth(self.config.minsize)

    def _transform_text(self, text: pcbnew.PCB_TEXT):
        if self._check_layer(text.GetLayer()):
            self.logger.debug("Transforming text '%s'", text.GetShownText())

            thickness = text.GetTextThickness()

            if thickness < self.config.minsize:
                self.logger.debug("Setting text thickness %d -> %d",
                                  thickness, self.config.minsize)
                text.SetTextThickness(self.config.minsize)

    def _transform_item(self, item: pcbnew.BOARD_ITEM):
        self.logger.debug(
            "Transforming item type %s (%s) at %s",
            item.Type(), item.GetTypeDesc(), item.GetPosition())

        if isinstance(item, pcbnew.FP_SHAPE):
            self.logger.debug("Item is FP_SHAPE")
            self._transform_fp_shape(item)

        elif isinstance(item, pcbnew.PCB_SHAPE):
            self.logger.debug("Item is PCB_SHAPE")
            self._transform_shape(item)

        elif isinstance(item, pcbnew.FOOTPRINT):
            for gi in item.GraphicalItems():
                self._transform_item(gi)
            self._transform_text(item.Reference())
            self._transform_text(item.Value())

        elif (isinstance(item, pcbnew.PCB_TEXT) or
              isinstance(item, pcbnew.FP_TEXT)):
            self.logger.debug("Item is FP/PCB_TEXT")
            self._transform_text(item)

    def _init_layers(self, board: pcbnew.BOARD):
        self.layers = (1 << pcbnew.F_SilkS) | (1 << pcbnew.B_SilkS)

    def Run(self):
        dialog = ConfigDialog()

        try:
            if dialog.ShowModal() != wx.ID_OK:
                return

            board: pcbnew.BOARD = pcbnew.GetBoard()
            self.config = dialog.GetConfig()
            self._init_layers(board)
            self._setup_logging(board.GetFileName())
            self.logger.debug("Running minsilk")

            for item in pcbnew.GetCurrentSelection():
                self._transform_item(item)
        finally:
            dialog.Destroy()
