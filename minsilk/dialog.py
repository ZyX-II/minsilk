import pcbnew

from dataclasses import dataclass

from .dialog_base import ConfigDialogBase


@dataclass
class Config:
    minsize: int = pcbnew.FromMM(0.15)
    debug_log: bool = False


class ConfigDialog(ConfigDialogBase):
    def __init__(self):
        ConfigDialogBase.__init__(self, None)

    def GetConfig(self) -> Config:
        return Config(
            minsize=pcbnew.FromMM(self.m_minWidth.Value),
            debug_log=self.m_debugLog.IsChecked(),
        )
