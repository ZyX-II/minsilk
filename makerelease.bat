cd %~dp0

set TAG=0

set ADIR=releases\%TAG%
set ARCHIVE=%ADIR%\minsilk.zip

rd /S /Q releases\tmp
rd /S /Q "%ADIR%"
mkdir "%ADIR%"

7z -tzip -mx9 a "%ARCHIVE%" minsilk "-x!*_test.py" "-x!*.pyc" "-x!*.log" "-x!*.ini" "-x!*.gitattributes" "-x!*__pycache__*"

mkdir releases\tmp\resources
copy icon\icon_64.png releases\tmp\resources\icon.png
copy metadata_template.json releases\tmp\metadata.json

cd releases\tmp
7z x ..\..\%ARCHIVE%
ren minsilk plugins

set PCM_ARCHIVE=..\..\%ADIR%\minsilk_v%TAG%_pcm.zip

7z -tzip -mx9 a "%PCM_ARCHIVE%" .
cd ..

rd /S /Q tmp
