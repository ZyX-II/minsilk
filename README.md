# MinSilk plugin for KiCad

![Icon](https://gitlab.com/ZyX-II/translit3/-/raw/icon/icon_large.png)

Change various objects on two silkscreen layers so that they have minimal line 
width. Should not affect objects not on silkscreen layers and objects with line 
width greater than or equal to the threshold.

This plugin only affects selected objects.

Plugin was created in response to most of footprints from KiCAD libraries 
violating design constraints of the manufacturer I use.

Objets changed:
- Shapes’ (e.g. graphical poligons’) lines width, but only if that width is 
  non-zero and shapes are not filled.
- Text thickness.
- Footprints are recursed into and have their contents, reference designator and 
  value text changed should any of that match criteria (layer and line width).

Note that after running minsilk on library footprint there is great chance it 
will produce DRC warning “footprint … does not match copy in library …”. This 
cannot be avoided.

Plugin needs to be rerun after updating footprints from library.

MinSilk plugin was created by taking [TransformIt][ti] as a template and 
changing most of its code.

[ti]: https://github.com/openscopeproject/TransformIt

# License

Plugin is distributed under MIT (Expat) license, see `LICENSE` for details.
